class Addstripekeys < ActiveRecord::Migration
  def change
    add_column :users, :stripe_test_secret_key, :string
    add_column :users, :stripe_test_publishable_key, :string
    add_column :users, :stripe_live_secret_key, :string
    add_column :users, :stripe_live_publishable_key, :string

    add_column :users, :stripe_test_mode, :boolean, default: true
  end
end
