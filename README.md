# Freelance Dance
An open source application for freelancers to manage their payments and workflow

## Requirements
* A Stripe account
* An AWS account (for attaching files)

## Application Requirements
* Ruby 2.3.3
* PostgreSQL

## How to install
* `bundle install`
* Create a `config/application.yml` based on the environment variables in `config/application.yml.sample`

## Contact
* freelancedance@stephwag.com
* http://www.freelancedance.net