class ApplicationMailer < ActionMailer::Base
  default from: "hello@freelancedance.net"
  layout 'mailer'
end
