class NotificationMailer < ApplicationMailer
  def client_email_milestone_create(email, token)
    @token = token
    mail(to: email, subject: "A milestone has been created for you on Freelancedance")
  end

  def client_email_milestone_update(email, token)
    @token = token
    mail(to: email, subject: "A milestone has been updated on Freelancedance")
  end
end
