# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  if($('#markdown-data').text().length > 0)
    text = $('#markdown-data').text()
    $('#markdown-data').html(markdown.toHTML(text))

  if($('#milestone-markdown-data').text().length > 0)
    text = $('#milestone-markdown-data').text()
    $('#milestone-markdown-data').html(markdown.toHTML(text))