class ProjectsController < ApplicationController
  def create
    @project = Project.new(project_params)
    @project.user = current_user
    respond_to do |format|
      if @project.save
        format.html { redirect_to "/projects/#{@project.id}", notice: "Project was created." }
      else
        format.html { redirect_to "/projects/new", alert: @project.errors.full_messages.join(", ") }
      end
    end
  end

  def show
    @project = Project.find(params[:id])
  end

  def edit
    @project = Project.find(params[:id])
  end

  def update
    @project = Project.find(params[:id])
    @project.assign_attributes(project_params)
    respond_to do |format|
      if @project.save
        format.html { redirect_to "/projects/#{@project.id}", notice: "Project was updated." }
      else
        format.html { redirect_to "/projects/#{@project.id}/edit", alert: @project.errors.full_messages.join(", ") }
      end
    end
  end

  private

  def project_params
    params.require(:project).permit(:name, :description, :client_email)
  end
end
