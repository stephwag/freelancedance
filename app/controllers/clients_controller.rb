class ClientsController < ApplicationController
  skip_before_action :authenticate_user!
  
  def project
    @project = Project.find_by(client_token: params[:client_token])
  end
end
