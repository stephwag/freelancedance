class MilestonesController < ApplicationController
  skip_before_filter :authenticate_user!, :only => [:show]
  def new
    @project = Project.find(params[:project_id])
  end

  def show
    @milestone = Milestone.find(params[:id])
  end

  def edit
    @milestone = Milestone.find(params[:id])
  end

  def mark_as_paid
    @milestone = Milestone.find(params[:milestone_id])
    @token = params[:stripeToken]
    @email = params[:stripeEmail]

    require "stripe"
    Stripe.api_key = current_user.stripe_secret_key

    Stripe::Charge.create(
      :amount => @milestone.price,
      :currency => "usd",
      :source => @token, # obtained with Stripe.js
      :description => "Milestone payment for '#{@milestone.name}' by #{@email}"
    )

    @milestone.update(paid: true)
    redirect_to "/clients/#{@milestone.project.client_token}", notice: "Milestone has been paid!"
  end

  def update
    @milestone = Milestone.find(params[:id])
    @milestone.assign_attributes(milestone_params)
    respond_to do |format|
      if @milestone.save

        if milestone_params[:update_email_client] == "1"
          NotificationMailer.client_email_milestone_update(@milestone.project.client_email, @milestone.project.client_token).deliver_now
        end

        format.html { redirect_to "/projects/#{@milestone.project.id}", notice: "Milestone was updated." }
      else
        format.html { redirect_to "/milestones/#{@milestone.id}/edit", alert: @milestone.errors.full_messages.join(", ") }
      end
    end
  end

  def create
    @project = Project.find(params[:project_id])
    @milestone = @project.milestones.new(milestone_params)
    respond_to do |format|
      if @milestone.save
        NotificationMailer.client_email_milestone_create(@milestone.project.client_email, @milestone.project.client_token).deliver_now
        format.html { redirect_to "/projects/#{@project.id}", notice: "Milestone was created." }
      else
        format.html { redirect_to "/projects/#{@project.id}/milestones/new", alert: @milestone.errors.full_messages.join(", ") }
      end
    end
  end

  private

  def milestone_params
    params.require(:milestone).permit(:name, :paid, :amount_complete, :description, :approved, :client_approved, :price, :update_email_client)
  end
end
