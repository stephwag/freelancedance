class HomeController < ApplicationController
  def index
    redirect_to "/users/sign_in"
  end

  def update_stripe_keys
    current_user.update(stripe_key_params)
    redirect_to "/users/edit", notice: "Successfully updated!"
  end

  private

  def stripe_key_params
    params.require(:user).permit(
      :stripe_test_secret_key, 
      :stripe_test_publishable_key,
      :stripe_live_secret_key,
      :stripe_live_publishable_key)
  end
end
