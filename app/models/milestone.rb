class Milestone < ActiveRecord::Base
  belongs_to :project
  validate :amount_complete_percentage

  attr_accessor :update_email_client

  def amount_complete_percentage
    if amount_complete < 0 || amount_complete > 100
      errors.add(:amount_complete, "must be between 0 and 100 percent")
    end
  end

  def display_price
    price / 100.0
  end
end
