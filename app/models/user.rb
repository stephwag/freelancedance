class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :projects

  def stripe_secret_key
    if stripe_test_mode
      stripe_test_secret_key
    else
      stripe_live_secret_key
    end
  end

  def stripe_publishable_key
    if stripe_test_mode
      stripe_test_publishable_key
    else
      stripe_live_publishable_key
    end
  end
end
